package db

import (
	"context"

	v1 "gitlab.com/sample-grpc/protos/tap-protos/types"
)



type db_struct struct{}

func (s db_struct) AppendData(ctx context.Context, req *v1.CreateRequest) (*v1.CreateResponse, error) {
	return &v1.CreateResponse{Res: []*v1.CreateRequest{req}}, nil
}
