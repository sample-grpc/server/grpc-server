package createservice

import (
	"context"

	v1 "gitlab.com/sample-grpc/protos/tap-protos/types"
)

type Store interface {
	AppendData(ctx context.Context, req *v1.CreateRequest) (*v1.CreateResponse, error)
}
