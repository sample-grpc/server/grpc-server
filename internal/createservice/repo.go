package createservice

import (
	"context"

	v1 "gitlab.com/sample-grpc/protos/tap-protos/types"
)

type GrpcServerServiceCal struct{}

var Data []*v1.CreateRequest

func (gssc GrpcServerServiceCal) CreateValueInSlice(ctx context.Context, req *v1.CreateRequest) (res *v1.CreateResponse, err error) {
	Data = append(Data, &v1.CreateRequest{Name: req.Name, Age: req.Age})
	return &v1.CreateResponse{Res: Data}, nil
}

func NewCreateService(r GrpcServerServiceCal) v1.CreateServiceServer {
	return r
}
