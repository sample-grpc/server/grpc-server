package getservice

import (
	"context"
	"grpc-server/internal/createservice"

	v1 "gitlab.com/sample-grpc/protos/tap-protos/types"
)

type GetServiceServer struct{}

func (g GetServiceServer) GetValueInSlice(ctx context.Context, req *v1.GetRequest) (*v1.GetResponse, error) {

	return &v1.GetResponse{Res: createservice.Data}, nil
}

func NewGetService(g GetServiceServer) v1.GetServiceServer {
	return g
}
