package main

import (
	"grpc-server/internal/createservice"
	"grpc-server/internal/getservice"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"

	v1 "gitlab.com/sample-grpc/protos/tap-protos/types"
	"google.golang.org/grpc"
)

func main() {

	listenAddr := ":50051" // Address to listen on
	lis, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	server := grpc.NewServer()

	// Create an instance of the server implementation
	// myServiceServer := &myServer{}

	// Register the server implementation with the gRPC server
	v1.RegisterCreateServiceServer(server, createservice.NewCreateService(createservice.GrpcServerServiceCal{}))
	v1.RegisterGetServiceServer(server, getservice.NewGetService(getservice.GetServiceServer{}))

	log.Printf("Server listening on %s", listenAddr)
	
	// Create a channel to receive the interrupt/termination signal
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM)

	// Start the gRPC server in a separate goroutine
	go func() {
		if err := server.Serve(lis); err != nil {
			log.Fatalf("Failed to serve: %v", err)
		}
	}()

	// Wait for the interrupt/termination signal
	<-stop

	// Gracefully stop the server
	server.GracefulStop()

}
